/*
 * S5WDial.cpp - part of DialWidget
 * Copyright © 2007, 2013, 2018, 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <QColor>
#include <QEasingCurve>
#include <QElapsedTimer>
#include <QFont>
#include <QFontMetrics>
#include <QLinearGradient>
#include <QMouseEvent>
#include <QPainter>
#include <QPainterPath>
#include <QPixmap>
//#include <QPrinter>
#include <QRectF>
#include <QRegion>
#include <QResizeEvent>
#include <QTimer>

#include <cmath>

#include "stackblur.h"
#include "math-support.h"
#include "S5WDial.h"

// This is probably broken due to the changes implemented for caching and other
// speedups.
//#define SHOW_BEZIER // Debugging code to quickly grab coords.

// Flesh out the constants.
const double S5WDial::FULL_RANGE = 260.0;
const double S5WDial::FULL_SCALE = 90 - FULL_RANGE / 2;
const double S5WDial::ZERO_SCALE = FULL_SCALE + FULL_RANGE;
const QRectF S5WDial::FACE_RECT(-98.0, -98.0, 196.0, 196.0);
const QRectF S5WDial::TICK_MARKS_RECT(-90.0, -90.0, 180.0, 180.0);
const double S5WDial::MAX_NEEDLE_SPEED = FULL_RANGE; // i.e. FULL_RANGE in 1 second.
const int S5WDial::FRAMES_PER_SECOND = 60; // Hopefully this will work fine.
const int ANIMATE_TIME_MS = 1000; // Double the default interval for CPU load meter.

struct S5WDial::Private {
    // Control points for any bezier curves, see renderDrawing() for more info.
    // Basically these get drawn and can be altered with left and right mouse button
    // if SHOW_BEZIER is defined in S5WDial.cpp
    QPoint m_ctrl1, m_ctrl2;

    // Caches the background for speed
    QPixmap background, needleCoverPixmap;

    // Base needle and cover paths on the 200x200 coordinate system, with the
    // needle pointing straight up (i.e. not rotated).
    QPainterPath needle, needleCover;

    QPainterPath positionedNeedle; // Scaled/translated/rotated

    // Used to animate from old to new value
    QEasingCurve aniCurve = QEasingCurve(QEasingCurve::InOutQuad);
    QElapsedTimer aniStartTime;
    double oldValue;

    Private() : m_ctrl1(-30, -30), m_ctrl2(30, -30)
    {
        // The path for the needle.
        needle.moveTo(10, -5);
        needle.cubicTo(3, -18, 2, -57, 5, -69);
        needle.cubicTo(2, -77, 1, -80, 0, -90);
        needle.cubicTo(-1, -80, -2, -77, -5, -69);
        needle.cubicTo(-2, -57, -3, -18, -10, -5);
        needle.lineTo(10, -5);

        // The loop over the needle in the middle.
        needleCover.moveTo(-19, 44);
        needleCover.lineTo(-27, 8);
        needleCover.cubicTo(-33, -37, 33, -37, 27, 8);
        needleCover.lineTo(19, 44);
    }
};

S5WDial::S5WDial(QWidget *parent)
  : QWidget(parent)
  , m_tickStep(5.0)
  , m_greenLow(-1)
  , m_greenHigh(-1)
  , m_redValue(-1)
  , m_animationTimer(new QTimer(this))
  , m_dblValue(75.0)
  , m_minValue(50.0)
  , m_maxValue(100.0)
  , m_label("No Sensor")
  , d(new Private)
{
    setBackgroundRole(QPalette::Light);

    connect(m_animationTimer, SIGNAL(timeout()), SLOT(moveNeedle()));

//  This function will save a PDF of the widget to /tmp/dial-printout.pdf, with
//    graphing lines attached as well.
//    QTimer::singleShot(0, this, SLOT(printOut()));
}

void S5WDial::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    renderDrawing(painter);
}

double S5WDial::currentRotation() const
{
    // m_dblValue used to account for animation.
    // But if m_dblValue ends up greater than FULL_RANGE / 2 + 5 or so
    // in either direction, we need to clamp it to prevent it from going
    // over-range.
    return clamp(degreesFromValue(m_dblValue), -(FULL_RANGE / 2 + 5), FULL_RANGE / 2 + 5);
}

QSize S5WDial::sizeHint() const
{
    return QSize(450, 450);
}

void S5WDial::drawTickMarks(QPainter &painter)
{
    // Wouldn't this suck? :)
    if(maximum() == minimum())
        return;

    QFont f("Noto Sans Condensed", 8, QFont::Light);
    f.setPointSizeF(7.5);
    painter.setFont(f);

    qreal tickSeparation;
    tickSeparation = FULL_RANGE * m_tickStep / (maximum() - minimum());

    int j = 0;

    QPen thickLinePen(Qt::black, 2);
    QPen blackLine(Qt::black);
    thickLinePen.setCapStyle(Qt::FlatCap);

    for(double i = minimum(); i <= maximum(); i += m_tickStep, ++j) {
        painter.save();

        qreal rotation = -FULL_RANGE / 2 + j * tickSeparation;
        painter.rotate(rotation);

        painter.setPen(thickLinePen);
        painter.drawLine(0, -98, 0, -90);
        painter.setPen(blackLine);

        // Now translate "up" the line, rotate back to normal, draw text, and
        // we're done.
        painter.save(); // 2

        QString text = QString::number(i);
        QRectF br = painter.fontMetrics().boundingRect(text);
        double offset = br.width() / 2 * qAbs(sin(qAbs(rotation * M_PI / 180.0))) +
                        br.height() / 2 * qAbs(cos(qAbs(rotation * M_PI / 180.0))) ;

        // Move the point of rotation to the center of the text box
        br.moveCenter(QPoint(0,0));

        painter.translate(0, -89 + offset);
        painter.rotate(-rotation);

        // If on the left side of the dial, use left-aligned text.
        // If on the top side, use centered.
        // If on the right side, use right-aligned text.
        int flags = Qt::AlignCenter; // middle/top case

        if(rotation > 45.0) { // left side
            flags = Qt::AlignLeft | Qt::AlignVCenter;
        }
        else if(rotation < 45.0) { // right side
            flags |= Qt::AlignRight | Qt::AlignVCenter;
        }

        painter.drawText(br, flags, text);
        painter.restore(); // 2

        if((i + m_tickStep / 2) < maximum()) {
            for(int k = 0; k < 4; ++k) {
                painter.rotate(tickSeparation / 5);
                painter.drawLine(QLineF(0, -90.8, 0, -95));
            }
        }

        painter.restore();
    }
}

double S5WDial::deltaToDegrees(double delta) const
{
    return delta * FULL_RANGE / (maximum() - minimum());
}

double S5WDial::degreesToDelta(double degrees) const
{
    return degrees * (maximum() - minimum()) / FULL_RANGE;
}

void S5WDial::setValue(int value)
{
    setValue(static_cast<double>(value));
}

void S5WDial::setValue(double val)
{
    if(val == value() && val == m_dblValue)
        return;

    if(m_setValue != val) {
        emit valueChanged(val);
        emit valueChanged(static_cast<int>(val));
    }

    d->oldValue = m_dblValue;
    m_setValue = val;

    if(!animationEnabled()) {
        m_dblValue = value();
        d->positionedNeedle = scaledNeedlePath(currentRotation());

        update(); // update face is not necessary here.
    }
    else {
        d->aniStartTime.start();
        m_animationTimer->start(1000 / FRAMES_PER_SECOND);
    }
}

void S5WDial::setGreenBand(double low, double high)
{
    m_greenLow = low;
    m_greenHigh = high;

    updateFace();
}

void S5WDial::moveNeedle()
{
    qint64 elapsed = d->aniStartTime.elapsed();
    double t = static_cast<double>(elapsed) / ANIMATE_TIME_MS;

    if(t >= 1.0) {
        // All done
        m_dblValue = value();
        m_animationTimer->stop();
    } else {
        const qreal prog = d->aniCurve.valueForProgress(t);
        m_dblValue = d->oldValue + prog * (m_setValue - d->oldValue);
    }

    d->positionedNeedle = scaledNeedlePath(currentRotation());
    update();
}

void S5WDial::setRedBand(double value, bool higherIsWorse)
{
    m_redValue = value;
    m_redIsHigher = higherIsWorse;

    updateFace();
}

void S5WDial::drawBand(QPainter &painter, QColor color, double l, double r)
{
    double delta = maximum() - minimum();
    if(delta == 0.0)
        return;

    int rBound = 16 * (FULL_SCALE + FULL_RANGE * (maximum() - r) / delta);
    int lBound = 16 * (FULL_SCALE + FULL_RANGE * (maximum() - l) / delta);

    painter.setPen(Qt::NoPen);
    painter.setBrush(color);
    painter.drawPie(FACE_RECT, rBound, lBound - rBound);

    painter.setBrush(Qt::white);
    painter.drawPie(TICK_MARKS_RECT, rBound - 16, lBound - rBound + 32);
}

void S5WDial::setTickInterval(double interval)
{
    m_tickStep = interval;
    updateFace();
}

void S5WDial::renderDrawingBackground(QPainter &painter, bool expand)
{
    painter.setRenderHint(QPainter::Antialiasing, m_antialiasingEnabled);

    // Don't fill with black if printing.
    if(!expand) {
        painter.setPen(Qt::NoPen);

        QLinearGradient backFace(-25, -100, 25, 100);
        backFace.setColorAt(0, QColor(75, 75, 75));
        backFace.setColorAt(1, QColor(35, 35, 35));
        backFace.setSpread(QGradient::ReflectSpread);
        painter.setBrush(backFace);
    }
    else {
        painter.setPen(Qt::black);
        painter.setBrush(Qt::NoBrush);
    }

    // Draw the case of the dial.
    QRectF caseRect = FACE_RECT.adjusted(-2, -2, 2, 2);
    painter.drawRoundedRect(caseRect, 15.0, 15.0);

    painter.setBrush(Qt::white);
    painter.drawEllipse(FACE_RECT);

    // Draw green band.
    if(m_greenHigh > m_greenLow) {
        double l = qMax(minimum(), m_greenLow);
        double h = qMin(maximum(), m_greenHigh);

        drawBand(painter, Qt::green, l, h);
    }

    // Draw red band.
    if(m_redValue != -1 && m_redValue < maximum() && m_redValue > minimum()) {
        if(m_redIsHigher)
            drawBand(painter, Qt::red, m_redValue, maximum());
        else
            drawBand(painter, Qt::red, minimum(), m_redValue);
    }

    painter.setBrush(Qt::NoBrush);
    painter.setPen(Qt::black);
    painter.drawEllipse(FACE_RECT); // Fill in edges of band

    drawTickMarks(painter);

    // Draw the inner circle part of the dial, where tick marks and needle
    // touch.
    painter.setPen(Qt::darkGray);
    painter.drawArc(TICK_MARKS_RECT, 16 * FULL_SCALE, 16 * FULL_RANGE);

    // Reset path
    QPainterPath path;

#ifdef SHOW_BEZIER
    // Draw bezier curve and controls points.  Use startPos and endPos as
    // endpoints, use m_ctrl1 and m_ctrl2 as control points.
    QPoint startPos(-27, 8), endPos(27, 8);
    path.moveTo(startPos);
    path.cubicTo(m_ctrl1, m_ctrl2, endPos);
    painter.strokePath(path, painter.pen());

    // Semi-transparent
    painter.setPen(QColor(0, 0, 0, 128));
    painter.drawLine(startPos, m_ctrl1);
    painter.drawLine(m_ctrl2, endPos);

    painter.setPen(QPen(Qt::black, 2));
    painter.drawPoint(startPos);
    painter.drawPoint(endPos);

    painter.setPen(QPen(Qt::red, 2));
    painter.drawPoint(m_ctrl1);
    painter.drawPoint(m_ctrl2);
#else
    painter.setPen(Qt::black);
    painter.setBrush(Qt::white);

    // Clip future drawing to the face of the dial.
    QPainterPath clipPath;
    clipPath.addEllipse(FACE_RECT);
    // Paths don't seem to play nice with clip regions. :(
    painter.setClipPath(clipPath);

    // Draw the top arc on the left side.
    path.moveTo(-73, 72);
    path.cubicTo(-52, 55, -38, 48, -19, 44);
    painter.strokePath(path, painter.pen());

    // Draw the top arc on the right side.
    path = QPainterPath();
    path.moveTo(19, 44);
    path.cubicTo(38, 48, 52, 55, 73, 72);
    painter.strokePath(path, painter.pen());

    // Add the shadow of the needle cover that won't be drawn yet.
    if(dropShadowEnabled()) {
        QImage cover(size(), QImage::Format_ARGB32_Premultiplied);
        QColor trans(Qt::transparent);
        cover.fill(trans.rgba());

        QPainter coverPainter;
        coverPainter.begin(&cover);
        coverPainter.setWorldTransform(translationTransform(width(), height()));
        coverPainter.setRenderHint(QPainter::Antialiasing, m_antialiasingEnabled);

        coverPainter.setPen(Qt::black);
        coverPainter.setBrush(Qt::white);
        coverPainter.drawPath(d->needleCover);

        coverPainter.end();
        stackBlur(cover, 1);

        QTransform t = translationTransform(width(), height());
        painter.setWorldMatrixEnabled(false);
        QRect pathRect = t.mapRect(d->needleCover.controlPointRect().toRect())
                          .adjusted(0, 0, 0, -1);
        painter.drawImage(pathRect.adjusted(1, 1, 1, 1), cover, pathRect);
        painter.setWorldMatrixEnabled(true);
    }

    QLinearGradient blackFace(0, 80, 0, 90);
    blackFace.setColorAt(0, Qt::black);
    blackFace.setColorAt(0.5, Qt::darkGray);
    blackFace.setColorAt(1, Qt::black);
    blackFace.setSpread(QGradient::ReflectSpread);

    // Draw the weird egg-shaped black part at bottom.
    painter.setBrush(blackFace);
    painter.setPen(Qt::black);
    painter.drawEllipse(-80, 67, 160, 80);
#endif

    // Draw the label.
    const QFont labelFont("Noto Sans Condensed", 7);
    const QFontMetrics labelMetrics(labelFont);
    const QRectF textRect(-98.0, 48, 2 * 98.0, 50);
    QRectF textPathRect(
            0.0, 0.0,
            labelMetrics.horizontalAdvance(m_label), labelMetrics.ascent());

    textPathRect.moveLeft(-textPathRect.center().x());
    textPathRect.moveTop(44 + labelMetrics.ascent());

    QPainterPath textPolys;
    textPolys.addText(textPathRect.topLeft(), labelFont, m_label);

    // Map the linear text baseline into a curve to better fit the panel
    for(int i = 0; i < textPolys.elementCount(); ++i) {
        const auto pt = textPolys.elementAt(i);
        const double baselineArc = m_arcValue;
        const double baselineRadius = m_radValue;
        // theta_p meant to point to where within baselineArc the pt.x should
        // be.
        double theta_p = 0.5 * baselineArc * abs(pt.x) / textPathRect.width();
        double xOnCurve = theta_p * baselineRadius * cos(theta_p / 2);
        double dy = theta_p * baselineRadius * sin(theta_p / 2);
        double newX = pt.x < 0 ? -xOnCurve : xOnCurve;
        double newY = pt.y + dy;
        textPolys.setElementPositionAt(i, newX, newY);
    }

    painter.setBrush(Qt::black);
    painter.setPen(Qt::transparent);
    painter.drawPath(textPolys);
}

void S5WDial::renderDrawing(QPainter &painter, bool expand)
{
    int w = painter.device()->width();
    int h = painter.device()->height();

    painter.setRenderHint(QPainter::Antialiasing, m_antialiasingEnabled);
    QTransform t = translationTransform(w, h);

    // Expand used if printing.
    if(expand) {
        // Printing, the cached background will not be useful.
        t.scale(0.75, 0.75);
        painter.setWorldTransform(t);
        renderDrawingBackground(painter, expand);
    }
    else {
        // Draw in cached background.
        painter.drawPixmap(rect(), d->background, d->background.rect());
    }

    // Get ready to draw the needle. We need to draw into a QImage for the
    // drop shadow so let's make yet another painter.

    // Rotation amount determined by value.
    double rotation = currentRotation();
    t.rotate(rotation);

    if(dropShadowEnabled()) {
        // this QImage needs to be size() large so we can draw into it without
        // transforming.
        QImage needleImage(size(), QImage::Format_ARGB32_Premultiplied);
        QColor trans(Qt::transparent);
        needleImage.fill(trans.rgba());

        QPainter needlePainter;

        needlePainter.begin(&needleImage);
        needlePainter.setRenderHint(QPainter::Antialiasing, m_antialiasingEnabled);

        needlePainter.setBrush(Qt::black);
        needlePainter.setPen(Qt::black);
        needlePainter.drawPath(d->positionedNeedle);

        needlePainter.end();

        // But we don't want to stackBlur an image this large, so let's copy
        // out the goodies.
        QRect boundingBox = d->positionedNeedle.controlPointRect().toRect();
        QImage shadowImage = needleImage.copy(boundingBox);

        // This code creates the image used for the shadow.
        stackBlur(shadowImage, 1);

        // Draw drop shadow in the real painter.
        QRect dstRect(boundingBox.adjusted(1, 1, 1, 1));
        painter.drawImage(dstRect, shadowImage, shadowImage.rect());
    }

    // Draw the needle
    painter.setBrush(QColor(50, 50, 50));
    painter.setPen(Qt::darkGray);

    painter.drawPath(d->positionedNeedle);

    // Draw line in middle of the needle for texture, use world matrix to
    // give correct scaling, translation, and rotation
    painter.setWorldTransform(t);
    painter.setPen(QPen(Qt::darkGray, 0));
    painter.drawLine(0, 0, 0, -84);

    // Disable transformations
    painter.setWorldMatrixEnabled(false);

    // Draw cover over the needle
    painter.drawPixmap(rect(), d->needleCoverPixmap, d->needleCoverPixmap.rect());
}

void S5WDial::setLabel(const QString &label)
{
    m_label = label;
    updateFace();
}

QString S5WDial::label() const
{
    return m_label;
}

void S5WDial::printOut()
{
#if 0
    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFileName("/tmp/dial-printout.pdf");
    printer.setFontEmbeddingEnabled(false);
    printer.setDocName("S5W Dial Graph Paper");
    printer.setCreator("S5WDial");

    QPainter painter;

    painter.begin(&printer);

    renderDrawing(painter, true);

    painter.setFont(QFont("Arial", 3));
    painter.setPen(QColor(0, 0, 0, 128));
    for(int i = -100; i <= 100; i += 10) {
        painter.drawLine(-100, i, 100, i);
        painter.drawLine(i, -100, i, 100);
        QString text(QString::number(i));
        QRectF horizLeft(-200, i - 30, 98, 60);
        QRectF horizRight(102, i - 30, 98, 60);
        QRectF vertTop(i - 100, -160, 200, 60);
        QRectF vertBot(i - 100, 102, 200, 60);

        painter.drawText(horizLeft, Qt::AlignVCenter | Qt::AlignRight, text);
        painter.drawText(horizRight, Qt::AlignVCenter | Qt::AlignLeft, text);
        painter.drawText(vertTop, Qt::AlignHCenter | Qt::AlignBottom, text);
        painter.drawText(vertBot, Qt::AlignHCenter | Qt::AlignTop, text);
    }

    painter.end();
#endif
}

QTransform S5WDial::translationTransform(qreal w, qreal h) const
{
    QTransform t;

    auto side = qMin(w, h); // Find shortest side

    // Adjust coordinates for ease of drawing.
    t.translate(w / 2, h / 2);
    t.scale(side / 200.0, side / 200.0);

    return t;
}

QPoint S5WDial::widgetToCoord(const QPoint &p) const
{
    QTransform t = translationTransform(width(), height()).inverted();
    return t.map(p);
}

void S5WDial::mouseMoveEvent(QMouseEvent *e)
{
    (void) e;
#ifdef SHOW_BEZIER
    if(e->buttons() & Qt::LeftButton) {
        m_ctrl1 = widgetToCoord(e->pos());
        update();
    }
    else if(e->buttons() & Qt::RightButton) {
        m_ctrl2 = widgetToCoord(e->pos());
        update();
    }
#endif
}

void S5WDial::mouseReleaseEvent(QMouseEvent *e)
{
    (void) e;
#ifdef SHOW_BEZIER
    if(e->button() == Qt::LeftButton)
        m_ctrl1 = widgetToCoord(e->pos());
    else
        m_ctrl2 = widgetToCoord(e->pos());

    update();
#endif
}

double S5WDial::minimum() const
{
    return m_minValue;
}

double S5WDial::value() const
{
    return m_setValue;
}

double S5WDial::maximum() const
{
    return m_maxValue;
}

void S5WDial::setMinimum(double min)
{
    if(min == m_minValue)
        return;

    m_minValue = min;
    updateFace();
}

void S5WDial::setMaximum(double max)
{
    if(max == m_maxValue)
        return;

    m_maxValue = max;
    updateFace();
}

double S5WDial::degreesFromValue(double value) const
{
    double delta = maximum() - minimum();
    if(delta == 0.0)
        return 0;

    // For the purposes of this function, 0 degrees is pointing straight up,
    // so positive rotation is a lesser value and negative rotation is a
    // greater value. (FULL_RANGE / 2) is the full range of deflection either
    // way.
    double rotation = ((value - minimum()) / delta - 0.5) * FULL_RANGE;
    return rotation;
}

void S5WDial::resizeEvent(QResizeEvent *)
{
    d->positionedNeedle = scaledNeedlePath(currentRotation());
    createCachedBackground();
}

void S5WDial::createCachedBackground()
{
    QImage background = QImage(size(), QImage::Format_ARGB32_Premultiplied);

    background.fill(qRgba(0, 0, 0, 0));

    QTransform t = translationTransform(width(), height());
    QPainter painter;

    // Cache the background
    painter.begin(&background);
    painter.setRenderHint(QPainter::Antialiasing, m_antialiasingEnabled);
    painter.setWorldTransform(t);

    renderDrawingBackground(painter);
    painter.end();

    d->background = QPixmap::fromImage(background);

    // Do same thing for the needle cover.
    background.fill(qRgba(0, 0, 0, 0));

    painter.begin(&background);
    painter.setRenderHint(QPainter::Antialiasing, m_antialiasingEnabled);
    painter.setWorldTransform(t);

    painter.setPen(Qt::black);
    painter.setBrush(Qt::white);

    painter.drawPath(d->needleCover);
    painter.end();

    d->needleCoverPixmap = QPixmap::fromImage(background);
}

void S5WDial::updateFace()
{
    createCachedBackground();
    update();
}

QPainterPath S5WDial::mapPath(QPainterPath path, const QTransform &t) const
{
    // Iterate through every point in the path, map it using the matrix, and
    // then set the path to the mapped value.
    for(int i = 0; i < path.elementCount(); ++i) {
        QPointF mapped = t.map(path.elementAt(i));

        path.setElementPositionAt(i, mapped.x(), mapped.y());
    }

    return path;
}

QPainterPath S5WDial::scaledNeedlePath(double rotation) const
{
    QTransform t = translationTransform(width(), height());
    t.rotate(rotation);
    return mapPath(d->needle, t);
}

bool S5WDial::dropShadowEnabled() const
{
    return m_dropShadowEnabled;
}

void S5WDial::setDropShadowEnabled(bool enable)
{
    if(m_dropShadowEnabled == enable)
        return;

    m_dropShadowEnabled = enable;
    updateFace(); // Need to update face for cover shadow.
}

bool S5WDial::animationEnabled() const
{
    return m_animationEnabled;
}

void S5WDial::setAnimationEnabled(bool enable)
{
    if(m_animationEnabled == enable)
        return;

    m_animationEnabled = enable;

    if(!m_animationEnabled && m_animationTimer->isActive()) {
        m_animationTimer->stop();
        setValue(value());
    }
}

void S5WDial::setRadValue(double rad)
{
    m_radValue = rad;
    updateFace();
}

void S5WDial::setArcValue(double arc)
{
    m_arcValue = arc;
    updateFace();
}

bool S5WDial::antialiasingEnabled() const
{
    return true;
}

void S5WDial::setAntialiasingEnabled(bool)
{
}
