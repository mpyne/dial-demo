#ifndef DIAL_MAINWINDOW_H
#define DIAL_MAINWINDOW_H
/*
 * mainwindow.h - part of DialWidget
 * Copyright © 2007, 2013, 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <QMainWindow>
#include <QElapsedTimer>

class S5WDial;
class QTimer;
class QSlider;
class CPULoadInfo;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent);

    enum SensorType { NoSensor, CPUSensor, NetRxSensor, SliderSensor,
                      CPULoadSensor, MemorySensor
                    };

    void setSensorType(SensorType type);

private slots:
    void updateNetData();
    void updateCPUTemp();
    void updateCPULoad();
    void updateMemoryLimits();
    void updateMemoryUsage();
    void slotToggleTracking(bool disableTracking);

private:
    quint64 currentRxBytes();
    bool readCPULoad(CPULoadInfo &load);

    class Private;
    Private *d;

    S5WDial *m_dial;
    QElapsedTimer m_timeElapsed;
    QTimer *m_timer;
    QSlider *m_arcSlider;
    QSlider *m_radSlider;
    QSlider *m_slider;
    quint64 m_bytesRx;
    SensorType m_sensorType;
};

#endif
