# S5W Dial Demo

## Description

Demos the dial widget I'm making.

![Screenshot](https://bitbucket.org/mpyne/dial-demo/raw/master/screenshot.png)

## Synopsis

    $ mkdir build
    $ cd build
    $ cmake ../
    $ make -j $(nproc)
    $ ./dial-demo [--cpu | --cpu-load | --net-rx | --slider | --memory]

## Notes

The sensors present here are essentially hardcoded for Linux. The `--cpu` sensor
(CPU temperature) probably doesn't even work at all anymore.

Rather than fixing this, I just want to make the widget available with "real"
sensor platforms.

My hardware changed aeons ago but `--cpu-load` and `--memory` works on basically any Linux
system so if you're just looking to see what the demo does, try that.  If you
don't specify a sensor then you can control dial output with a slider instead.

I don't know how to do BSD sensors, sorry. :)

## Dependencies

Pretty much any [Qt](https://www.qt.io/) 5 should work. You will need to use
[CMake](https://cmake.org/) to make the build system, though you can probably
get away with `qmake -project && qmake` if you don't have CMake for some
reason.

Note: The reference font is Google's [Noto Sans
Condensed](https://www.google.com/get/noto/). Other fonts should at least show
up but may break the layout.

## TODO

- Integrate with ksysguard instead of writing my own damn sensors lib.
- CPU optimizations aplenty.
- **HELP WANTED**: Improve the "curved label" at the bottom. The two sliders in
  the screenshot are intended to help choose appropriate constants for the
  current shaping algorithm but to be honest it's pretty crap. I feel like
  there has to be an easier way to take a set of rectangular text path polygons
  and curve them to cleanly take a circular path but the math isn't quite
  working for me and I didn't see any easy demonstrations online.

## Aside

- Why S5WDial.cpp?  The S5W plant is the oldest submarine nuclear plant I know of that
  used the [Westinghouse KX-241 type of
  dials](https://www.weschler.com/product/dc-voltage-current-weschler/) (and
  variants).  Older nuclears plants probably used them as well since they're so
  awesome but I have no practical experience with them.

## Author

Michael Pyne <mpyne@kde.org>
