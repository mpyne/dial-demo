/*
 * main.cpp - part of DialWidget
 * Copyright © 2007, 2013, 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <QApplication>
#include <QStringList>
#include "S5WDial.h"
#include "mainwindow.h"

// Version: 0.3.0

int main(int argc, char **argv)
{
    QApplication a(argc, argv);
    QStringList args = a.arguments();

    a.setApplicationName("DialWidget");

    MainWindow *mw = new MainWindow(0);

    // Default sensor, also used with --slider.
    MainWindow::SensorType type = MainWindow::SliderSensor;

    if(args.count() > 1) {
        if(args.at(1) == "--cpu")
            type = MainWindow::CPUSensor;
        else if(args.at(1) == "--net-rx")
            type = MainWindow::NetRxSensor;
        else if(args.at(1) == "--slider")
            type = MainWindow::SliderSensor;
        else if(args.at(1) == "--cpu-load")
            type = MainWindow::CPULoadSensor;
        else if(args.at(1) == "--memory")
            type = MainWindow::MemorySensor;
    }

    mw->setSensorType(type);
    mw->show();

    return a.exec();
}
