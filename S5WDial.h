#ifndef WIDGET_S5WDIAL
#define WIDGET_S5WDIAL
/*
 * S5WDial.h - part of DialWidget
 * Copyright © 2007, 2013, 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <QWidget>
#include <QString>
#include <QRectF>

class QTimer;
class QTransform;
class QPainterPath;

class S5WDial : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QString label READ label WRITE setLabel)
    Q_PROPERTY(double minimum READ minimum WRITE setMinimum)
    Q_PROPERTY(double maximum READ maximum WRITE setMaximum)
    Q_PROPERTY(double value READ value WRITE setValue)
    Q_PROPERTY(bool antialiasingEnabled READ antialiasingEnabled WRITE setAntialiasingEnabled)
    Q_PROPERTY(bool dropShadowEnabled READ dropShadowEnabled WRITE setDropShadowEnabled)
    Q_PROPERTY(bool animationEnabled READ animationEnabled WRITE setAnimationEnabled)

public:
    S5WDial(QWidget *parent);

    void setGreenBand(double low, double high);
    void setRedBand(double value, bool higherIsWorse = true);
    void setTickInterval(double interval);

    QString label() const;
    double minimum() const;
    double maximum() const;
    double value() const;

    bool antialiasingEnabled() const;
    bool animationEnabled() const;
    bool dropShadowEnabled() const;

    QSize sizeHint() const;

// Constants for use in calculations.
    // Number of degrees from min to max.
    static const double FULL_RANGE;

    // Number of degrees to full scale. i.e. starting from 0 degrees (points
    // to the right), how many degress to rotate to be pointing full scale.
    static const double FULL_SCALE;

    // Number of degrees to rotate to be pointing at minimum scale.  We add
    // degrees since counter-clockwise is positive.
    static const double ZERO_SCALE;

    // Rectangle enclosing the circle for the face of the dial.
    static const QRectF FACE_RECT;

    // Rectangle enclosing the circle for the tick marks.
    static const QRectF TICK_MARKS_RECT;

    // Maximum amount to move the dial per second.
    static const double MAX_NEEDLE_SPEED;

    // Number of frames to animate per second.
    static const int FRAMES_PER_SECOND;

public slots:
    void setMaximum(double max);
    void setMinimum(double min);
    void setValue(double val);
    void setLabel(const QString &label);
    void setValue(int value);
    void setAntialiasingEnabled(bool enable);
    void setDropShadowEnabled(bool enable);
    void setAnimationEnabled(bool enable);

    void setRadValue(double rad);
    void setArcValue(double arc);

signals:
    void mouseTrack(QPoint pos);
    void valueChanged(double newVal);
    void valueChanged(int newVal);

protected:
    virtual void paintEvent(QPaintEvent *e);

    // Used for SHOW_BEZIER stuff.
    virtual void mouseMoveEvent(QMouseEvent *e);
    virtual void mouseReleaseEvent(QMouseEvent *e);
    virtual void resizeEvent(QResizeEvent *e);

    inline double degreesFromValue(double value) const;

    // Convert to/from a delta-value (i.e. change of 5 degrees Celsius) to the
    // equivalent deflection on the meter in degrees.
    inline double deltaToDegrees(double delta) const;
    inline double degreesToDelta(double degrees) const;

private slots:
    void printOut();
    void moveNeedle();

private:
    inline double currentRotation() const;
    void renderDrawingBackground(QPainter &painter, bool expand = false);
    void renderDrawing(QPainter &painter, bool expand = false);
    void drawTickMarks(QPainter &painter);
    void drawBand(QPainter &painter, QColor color, double l, double r);
    QTransform translationTransform(qreal w, qreal h) const;
    QPoint widgetToCoord(const QPoint &p) const;
    QPainterPath scaledNeedlePath(double rotation) const;
    void createCachedBackground();
    void updateFace();
    QPainterPath mapPath(QPainterPath path, const QTransform &t) const;

private: // members
    class Private;

    double m_tickStep;

    double m_greenLow, m_greenHigh;
    double m_redValue;
    bool m_redIsHigher;
    bool m_antialiasingEnabled = true;
    bool m_dropShadowEnabled   = true;
    bool m_animationEnabled    = true;

    QTimer *m_animationTimer;

    double m_radValue = 0;
    double m_arcValue = 0;

    double m_dblValue; // The actual value at this time.
    double m_setValue; // The value which is set as true.  May not be the
                       // current value if the needle is taking time to catch
                       // up.
    double m_minValue, m_maxValue;

    QString m_label;

    Private *d;
};

#endif
