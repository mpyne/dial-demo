/*
 * mainwindow.cpp - part of DialWidget
 * Copyright © 2007, 2013, 2018, 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "mainwindow.h"

#include <algorithm>
#include <cmath>

#include <QCheckBox>
#include <QDir>
#include <QFile>
#include <QHBoxLayout>
#include <QLabel>
#include <QPoint>
#include <QRegExp>
#include <QSlider>
#include <QSpinBox>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QTimer>
#include <QVBoxLayout>

#include "S5WDial.h"

struct CPULoadInfo
{
    quint64 userTicks, sysTicks, niceTicks, idleTicks;
};

template <class T>
static T averageValue(const QVector<T> &list)
{
    T sum = std::accumulate(list.begin(), list.end(), 0.0);
    return sum / list.count();
}

class MainWindow::Private
{
    public:

    CPULoadInfo lastLoad;
    QVector<double> lastThreeLoads;
    quint64 memLevel;
};

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    d(new MainWindow::Private), m_timer(new QTimer(this)),
    m_sensorType(NoSensor)
{
    QWidget *w = new QWidget(this);
    setCentralWidget(w);

    QBoxLayout *v = new QVBoxLayout(w);

    m_dial = new S5WDial(w);
    v->addWidget(m_dial, 1);

    m_slider = new QSlider(Qt::Horizontal, w);
    v->addWidget(m_slider);
    m_slider->hide(); // Only show in SliderSensor mode.
    connect(m_dial, SIGNAL(valueChanged(int)), m_slider, SLOT(setValue(int)));
    connect(m_slider, SIGNAL(valueChanged(int)), m_dial, SLOT(setValue(int)));

    m_arcSlider = new QSlider(Qt::Horizontal, w);
    v->addWidget(m_arcSlider);
    m_arcSlider->setRange(5, 300);
    m_arcSlider->setToolTip(QLatin1String("Amount of circular arc full label should reach (radians)"));
    connect(m_arcSlider, &QSlider::valueChanged, m_dial, [this](int val) {
                m_dial->setArcValue(val / 100.0);
            });
    m_arcSlider->setValue(98);

    m_radSlider = new QSlider(Qt::Horizontal, w);
    v->addWidget(m_radSlider);
    m_radSlider->setRange(30, 500);
    m_radSlider->setToolTip(QLatin1String("Radius of logical circle to wrap around"));
    connect(m_radSlider, &QSlider::valueChanged, m_dial, [this](int val) {
                m_dial->setRadValue(val * 1.0);
            });
    m_radSlider->setValue(168);

    QCheckBox *cb = new QCheckBox("Enable drop shadow", w);
    v->addWidget(cb);
    connect(cb, SIGNAL(toggled(bool)), m_dial, SLOT(setDropShadowEnabled(bool)));
    cb->setChecked(m_dial->dropShadowEnabled());

    cb = new QCheckBox("Enable animation", w);
    v->addWidget(cb);
    connect(cb, SIGNAL(toggled(bool)), m_dial, SLOT(setAnimationEnabled(bool)));

    // Disable tracking when animation is enabled.
    connect(cb, SIGNAL(toggled(bool)), SLOT(slotToggleTracking(bool)));
    cb->setChecked(m_dial->animationEnabled());

    m_dial->setLabel("No Sensor");
    m_dial->setMinimum(0);
    m_dial->setMaximum(100);
    m_dial->setValue(50);
    m_dial->setTickInterval(10);

    m_slider->setRange(m_dial->minimum(), m_dial->maximum());
    m_slider->setValue(m_dial->value());
    m_slider->setTracking(!m_dial->animationEnabled());
}

void MainWindow::slotToggleTracking(bool disableTracking)
{
    m_slider->setTracking(!disableTracking);
}

void MainWindow::setSensorType(SensorType type)
{
    if(type == m_sensorType)
        return;

    m_sensorType = type;
    m_timer->disconnect(this);
    m_timer->stop();
    m_slider->hide();

    switch(m_sensorType) {
        case CPUSensor:
            m_dial->setMaximum(120);
            m_dial->setMinimum(30);
            m_dial->setTickInterval(10.0);
            m_dial->setValue(50.0);
            m_dial->setRedBand(85);
            m_dial->setLabel(QString::fromUtf8("CPU (°C)"));

            updateCPUTemp();

            connect(m_timer, SIGNAL(timeout()), SLOT(updateCPUTemp()));
            m_timer->start(2000);
            break;

        case NetRxSensor:
            m_bytesRx = currentRxBytes();

            m_dial->setMaximum(1000);
            m_dial->setTickInterval(100.0);
            m_dial->setMinimum(0);
            m_dial->setValue(0.0);
            m_dial->setLabel("RX (KBps)");
            m_timeElapsed.start();

            connect(m_timer, SIGNAL(timeout()), SLOT(updateNetData()));
            m_timer->start(250);
            break;

        case SliderSensor:
            m_dial->setMinimum(0);
            m_dial->setMaximum(200);
            m_dial->setValue(100);
            m_dial->setLabel("No Sensor");
            m_dial->setTickInterval(20);

            m_slider->show();
            break;

        case CPULoadSensor:
            readCPULoad(d->lastLoad);

            m_dial->setMinimum(0);
            m_dial->setMaximum(100);
            m_dial->setLabel("CPU Load (%)");
            m_dial->setTickInterval(10);

            connect(m_timer, SIGNAL(timeout()), SLOT(updateCPULoad()));
            m_timer->start(500);

        case MemorySensor:
            updateMemoryLimits();
            updateMemoryUsage();
            connect(m_timer, &QTimer::timeout, this, &MainWindow::updateMemoryUsage);
            m_timer->start(500);

        default:
            // Nothing
            break;
    }

    m_slider->setRange(m_dial->minimum(), m_dial->maximum());
}

bool MainWindow::readCPULoad(CPULoadInfo &load)
{
    QFile stats("/proc/stat");

    if(!stats.open(QIODevice::ReadOnly))
        return false;

    QTextStream ts(&stats);
    QString cpuInfo = ts.readLine(); // First line is overall CPU stats.

    QRegExp infoExtractor("^cpu\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)");
    if(infoExtractor.indexIn(cpuInfo) == -1) {
        // Can't parse the string.
        return false;
    }

    load.userTicks = infoExtractor.cap(1).toULongLong();
    load.sysTicks = infoExtractor.cap(2).toULongLong();
    load.niceTicks = infoExtractor.cap(3).toULongLong();
    load.idleTicks = infoExtractor.cap(4).toULongLong();

    return true;
}

// Code below adapted from ksysguard, written by Chris Schlaeger (GPLv2).
void MainWindow::updateCPULoad()
{
    CPULoadInfo cur;

    if(!readCPULoad(cur))
        return;

    quint64 deltaTicks = cur.userTicks - d->lastLoad.userTicks +
                         cur.sysTicks - d->lastLoad.sysTicks +
                         cur.niceTicks - d->lastLoad.niceTicks +
                         cur.idleTicks - d->lastLoad.idleTicks;

    if (deltaTicks == 0) {
        d->lastThreeLoads.push_back(0);
    }
    else {
        double totalLoad = 100 - 100.0 * (cur.idleTicks - d->lastLoad.idleTicks)
                                       / deltaTicks;
        d->lastThreeLoads.push_back(totalLoad);
    }

    d->lastLoad = cur;
    if(d->lastThreeLoads.count() > 3)
        d->lastThreeLoads.pop_front();

    m_dial->setValue(averageValue(d->lastThreeLoads));
}

void MainWindow::updateMemoryLimits()
{
    QFile f("/proc/meminfo");

    if(!f.open(QIODevice::ReadOnly)) {
        qDebug("Unable to open memory file!");
        return;
    }

    QTextStream ts(&f);

    quint64 total = 0;
    QString data = ts.readLine();

    while(!data.isEmpty()) {
        QRegExp nameRE("^([\\sa-zA-Z:0-9]+):\\s*([0-9]*) ");

        if(nameRE.indexIn(data) == -1) {
            qDebug("Wrong name?");
            return;
        }

        const QString field = nameRE.cap(1).simplified();
        if(field == "MemTotal") {
            total = nameRE.cap(2).simplified().toULongLong();
            break;
        }

        data = ts.readLine();
    }

    d->memLevel = 1;
    int level = 0;
    double human_readable_total = total;
    while(level < 3 && human_readable_total > 1000) {
        d->memLevel *= 1024;
        level++;
        human_readable_total /= 1024;
    }

    const char *const levels[] = {
        " (KB)", // /proc/meminfo starts with KB first
        " (MB)",
        " (GB)",
        " (TB)",
    };

    const auto int_max = std::ceil(human_readable_total);
    m_dial->setValue(int_max);
    m_dial->setMinimum(0);
    m_dial->setMaximum(int_max);
    m_dial->setRedBand(int_max / 8, false /* higher values aren't red */);
    m_dial->setLabel(QString("Mem Avail%1").arg(QLatin1String(levels[level])));
    m_dial->setTickInterval(int_max / 8);
}

void MainWindow::updateMemoryUsage()
{
    QFile f("/proc/meminfo");

    if(!f.open(QIODevice::ReadOnly))
        return;

    QTextStream ts(&f);

    quint64 avail = 0;
    QString data = ts.readLine(); // This proc file is always EOF, just read it

    while(!data.isEmpty() && !avail) {
        QRegExp nameRE("^([\\sa-zA-Z:0-9]+):\\s*([0-9]*) ");

        if(nameRE.indexIn(data) == -1) {
            qDebug("Wrong name?");
            return;
        }

        const QString field = nameRE.cap(1).simplified();
        if(field == "MemAvailable") {
            avail = nameRE.cap(2).simplified().toULongLong();
            break;
        }

        data = ts.readLine();
    }

    m_dial->setValue(1.0 * avail / d->memLevel);
}

void MainWindow::updateCPUTemp()
{
    QDir sysI2C("/sys/bus/i2c/devices/");

    // Find list of sensors.
    QStringList I2CDevices = sysI2C.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    if(I2CDevices.isEmpty())
        return; // No devices, nothing I can do for now.

    // Just pick the first device.  I assume that temp1_input is the one we
    // want.  A real GUI would need scaling factors and offsets and a way to
    // guess the right temperature because lm_sensors is crap.
    QString devFileName = sysI2C.absoluteFilePath(I2CDevices.first()) + "/temp1_input";
    QFile f(devFileName);

    if(!f.open(QIODevice::ReadOnly))
        return;

    QTextStream ts(&f);

    QString l = ts.readLine(); // Read in value.
    bool ok = true;
    long val = l.simplified().toLong(&ok);

    if(!ok)
        return;

    // Works on my VIA motherboard (it8712 chip)
    double temp = val / 1000.0 * 1.4893 - 15.096;
    m_dial->setValue(temp);
}

void MainWindow::updateNetData()
{
    quint64 cur = currentRxBytes();
    quint64 delta = cur - m_bytesRx;
    m_bytesRx = cur;

    // We have delta, find time elapsed.
    qint64 msecsElapsed = m_timeElapsed.restart();

    if(msecsElapsed == 0) {
        qDebug("No time elapsed for measurement.");
        return;
    }

    double KBps = (delta * 1000.0 / 1024.0) / msecsElapsed;
    m_dial->setValue(KBps);
}

quint64 MainWindow::currentRxBytes()
{
    QFile f("/proc/net/dev");

    if(!f.open(QIODevice::ReadOnly))
        return 0;

    QTextStream ts(&f);

    ts.readLine();
    ts.readLine(); // Skip first 2 lines, which is header information.

    while(!ts.atEnd()) {
        QString data = ts.readLine();
        QRegExp nameRE("^([\\sa-zA-Z:0-9]+):\\s*(.*)$");

        if(nameRE.indexIn(data) == -1) {
            qDebug("Wrong name?");
            return 0;
        }

        // Change this to the device you'd like to query.
        if(nameRE.cap(1).simplified() != "eth0" && !nameRE.cap(1).startsWith("en"))
            continue;

        QStringList dataParts = nameRE.cap(2).simplified().split(QRegExp("\\s+"));
        if(!dataParts.isEmpty()) {
            return dataParts[0].toULongLong();
        }
        else {
            qDebug("No data with line.");
            return 0;
        }
    }

    return 0;
}
